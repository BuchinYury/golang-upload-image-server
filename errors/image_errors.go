package errors

const (
	EmptyImageError        = "image empty error"
	BadImageError          = "bad image error"
	ImageSaveError         = "image save error"
	NotSupportedImageError = "image note supported error"
)
