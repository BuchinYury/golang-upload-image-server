#!/usr/bin/env bash

cd /go/src/golang-upload-image-server/
export GO111MODULE=on
go build
./golang-upload-image-server -scp ./server-config.json