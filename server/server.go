package server

import (
	"encoding/json"
	"errors"
	"fmt"
	"golang-upload-image-server/handlers"
	"golang-upload-image-server/images"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

type Config struct {
	Port            string                 `json:"port"`
	PublicFilePath  string                 `json:"publicFilePath"`
	ImageUnitConfig images.ImageUnitConfig `json:"imageUnitConfig"`
}

func (c Config) Validate() error {
	if c.Port == "" {
		return errors.New("отсутствует port")
	}
	if c.PublicFilePath == "" {
		return errors.New("отсутствует publicFilePath")
	}

	err := c.ImageUnitConfig.Validate()
	if err != nil {
		return err
	}

	return nil
}

type Server struct {
	httpServer http.Server
}

func ParseServerConfig(serverConfigPath string) (Config, error) {
	serverConfigFile, err := os.Open(serverConfigPath)
	if err != nil {
		return Config{}, err
	}
	defer serverConfigFile.Close()

	serverConfigBytes, err := ioutil.ReadAll(serverConfigFile)
	if err != nil {
		return Config{}, err
	}

	var serverConfig Config
	err = json.Unmarshal(serverConfigBytes, &serverConfig)
	if err != nil {
		return Config{}, err
	}

	return serverConfig, nil
}

func NewServer(serverConfig Config) Server {
	imageUnit := images.ImageUnit{
		ImageUnitConfig: serverConfig.ImageUnitConfig,
	}

	rootHandler := handlers.RootHandler{
		PublicFilePath:  serverConfig.PublicFilePath,
		RootHandler:     handlers.IndexHandler("templates/index.html", &imageUnit),
		NotFoundHandler: handlers.NotFoundHandler("templates/404.html"),
	}

	uploadImageHandler := handlers.UploadImageHandler{
		UploadImageTemplate: template.Must(template.ParseFiles("templates/upload_image.html")),
		Client: http.Client{
			Timeout: 6 * time.Second,
		},
		ImageMetaGenerator: &imageUnit,
		ImageSaver:         &imageUnit,
		ImageResizer:       &imageUnit,
	}

	mux := http.NewServeMux()
	mux.Handle("/", rootHandler)
	mux.Handle("/upload_image", uploadImageHandler)

	httpServer := http.Server{
		Addr:         ":" + serverConfig.Port,
		Handler:      mux,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	return Server{
		httpServer: httpServer,
	}
}

func (server Server) Run() {
	fmt.Println("starting Server at", server.httpServer.Addr)
	err := server.httpServer.ListenAndServe()

	if err != nil {
		log.Fatal(err)
	}
}
