package server

import (
	"encoding/json"
	"golang-upload-image-server/images"
	"io/ioutil"
	"os"
	"testing"
)

func Test_Validate_ServerConfig(t *testing.T) {
	for _, testCase := range []struct {
		serverConfig Config
		isErr        bool
	}{
		{
			serverConfig: Config{
				Port:           "8080",
				PublicFilePath: "client",
				ImageUnitConfig: images.ImageUnitConfig{
					CustomersImagePath:    "client/images/customers",
					OriginalImagePathName: "original",
					PreviewImageUnitConfig: images.PreviewImageUnitConfig{
						PreviewImagePathName: "preview_100x100",
						ResizeImageHeight:    100,
						ResizeImageWidth:     100,
					},
				},
			},
			isErr: false,
		},
		{
			serverConfig: Config{},
			isErr:        true,
		},
		{
			serverConfig: Config{
				Port:           "8080",
				PublicFilePath: "",
				ImageUnitConfig: images.ImageUnitConfig{
					CustomersImagePath:    "client/images/customers",
					OriginalImagePathName: "original",
					PreviewImageUnitConfig: images.PreviewImageUnitConfig{
						PreviewImagePathName: "preview_100x100",
						ResizeImageHeight:    100,
						ResizeImageWidth:     100,
					},
				},
			},
			isErr: true,
		},
		{
			serverConfig: Config{
				Port:           "8080",
				PublicFilePath: "client",
				ImageUnitConfig: images.ImageUnitConfig{
					CustomersImagePath:    "",
					OriginalImagePathName: "original",
					PreviewImageUnitConfig: images.PreviewImageUnitConfig{
						PreviewImagePathName: "preview_100x100",
						ResizeImageHeight:    100,
						ResizeImageWidth:     100,
					},
				},
			},
			isErr: true,
		},
		{
			serverConfig: Config{
				Port:           "8080",
				PublicFilePath: "client",
				ImageUnitConfig: images.ImageUnitConfig{
					CustomersImagePath:    "client/images/customers",
					OriginalImagePathName: "",
					PreviewImageUnitConfig: images.PreviewImageUnitConfig{
						PreviewImagePathName: "preview_100x100",
						ResizeImageHeight:    100,
						ResizeImageWidth:     100,
					},
				},
			},
			isErr: true,
		},
		{
			serverConfig: Config{
				Port:           "8080",
				PublicFilePath: "client",
				ImageUnitConfig: images.ImageUnitConfig{
					CustomersImagePath:    "client/images/customers",
					OriginalImagePathName: "original",
					PreviewImageUnitConfig: images.PreviewImageUnitConfig{
						PreviewImagePathName: "",
						ResizeImageHeight:    100,
						ResizeImageWidth:     100,
					},
				},
			},
			isErr: true,
		},
		{
			serverConfig: Config{
				Port:           "8080",
				PublicFilePath: "client",
				ImageUnitConfig: images.ImageUnitConfig{
					CustomersImagePath:    "client/images/customers",
					OriginalImagePathName: "original",
					PreviewImageUnitConfig: images.PreviewImageUnitConfig{
						PreviewImagePathName: "preview_100x100",
						ResizeImageHeight:    0,
						ResizeImageWidth:     100,
					},
				},
			},
			isErr: true,
		},
		{
			serverConfig: Config{
				Port:           "8080",
				PublicFilePath: "client",
				ImageUnitConfig: images.ImageUnitConfig{
					CustomersImagePath:    "client/images/customers",
					OriginalImagePathName: "original",
					PreviewImageUnitConfig: images.PreviewImageUnitConfig{
						PreviewImagePathName: "preview_100x100",
						ResizeImageHeight:    100,
						ResizeImageWidth:     0,
					},
				},
			},
			isErr: true,
		},
	} {
		serverConfigValidate := testCase.serverConfig.Validate()

		if testCase.isErr && serverConfigValidate == nil {
			t.Error(serverConfigValidate)
			t.Error("неожиданный результат")
		}
	}
}

func Test_ParseServerConfig(t *testing.T) {
	for _, testCase := range []struct {
		serverConfigPath string
		isErr            bool
	}{
		{
			serverConfigPath: "./server_test.go",
			isErr:            true,
		},
		{
			serverConfigPath: "./",
			isErr:            true,
		},
		{
			serverConfigPath: "",
			isErr:            true,
		},
		{
			serverConfigPath: "./Test_ParseServerConfig/server-config.json",
			isErr:            false,
		},
	} {
		if !testCase.isErr {
			err := os.MkdirAll("./Test_ParseServerConfig", os.ModePerm)
			if err != nil {
				t.Fatal("нет возможности создать папку для теста")
			}

			serverConfigJsonByte, err := json.Marshal(Config{})
			if err != nil {
				t.Fatal("нет возможности создать файл для теста")
			}
			err = ioutil.WriteFile(testCase.serverConfigPath, serverConfigJsonByte, 0644)
			if err != nil {
				t.Fatal("нет возможности создать файл для теста")
			}
		}

		_, err := ParseServerConfig(testCase.serverConfigPath)

		if testCase.isErr && err == nil {
			t.Error("неожиданный результат")
		}

		if !testCase.isErr {
			err := os.RemoveAll("./Test_ParseServerConfig")
			if err != nil {
				t.Fatal("нет возможности создать папку для теста")
			}
		}
	}
}
