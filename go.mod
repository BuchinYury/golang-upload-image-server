module golang-upload-image-server

go 1.12

require (
	github.com/asaskevich/govalidator v0.0.0-20190424111038-f61b66f89f4a
	github.com/disintegration/imaging v1.6.0
	github.com/google/uuid v1.1.1
)
