package main

import (
	"flag"
	"golang-upload-image-server/server"
	"log"
)

func main() {
	serverConfigPathArgsFlag := "scp"
	serverConfigPathEmptyValue := "NOPE"
	serverConfigPath := flag.String(serverConfigPathArgsFlag, serverConfigPathEmptyValue, "")
	flag.Parse()

	if *serverConfigPath != serverConfigPathEmptyValue {
		runServer(*serverConfigPath)
	} else {
		argsNotContainsServerConfigPathMsg := "Не передан путь к файлу конфигурации сервера через флаг -" +
			serverConfigPathArgsFlag

		log.Fatal(argsNotContainsServerConfigPathMsg)
	}
}

func runServer(serverConfigPath string) {
	serverConfig, err := server.ParseServerConfig(serverConfigPath)
	if err != nil {
		log.Fatal(err)
	}
	err = serverConfig.Validate()
	if err != nil {
		log.Fatal(err)
	}

	server.NewServer(serverConfig).Run()
}
