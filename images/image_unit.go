package images

import (
	"errors"
	"github.com/google/uuid"
	serverErr "golang-upload-image-server/errors"
	"strings"
)

const (
	jpegExt = ".jpeg"
	pngExt  = ".png"
)

const (
	jpegMagic = "\xff\xd8\xff"
	pngMagic  = "\x89PNG\r\n\x1a\n"
)

var magicTable = map[string]string{
	jpegMagic: jpegExt,
	pngMagic:  pngExt,
}

type ImageTask struct {
	ImagePath string
	ImageName string
	ImageData []byte
}

type ImageUnit struct {
	ImageUnitConfig ImageUnitConfig
}

type ImageMetaGenerator interface {
	GenImageMeta() (string, string)
}

func (iu ImageUnit) GenImageMeta() (imagePath string, imageName string) {
	imagePath = iu.ImageUnitConfig.CustomersImagePath + "/" + uuid.New().String()
	imageName = "image-" + uuid.New().String()
	return
}

func SupportImageExtension(fileBytes []byte) (extension string, err error) {
	fileBytesStr := string(fileBytes)
	for magic, extension := range magicTable {
		if strings.HasPrefix(fileBytesStr, magic) {
			return extension, nil
		}
	}
	return "", errors.New(serverErr.NotSupportedImageError)
}

type ImageUnitConfig struct {
	CustomersImagePath     string                 `json:"customersImagePath"`
	OriginalImagePathName  string                 `json:"originalImagePathName"`
	PreviewImageUnitConfig PreviewImageUnitConfig `json:"previewImageUnitConfig"`
}

func (iuc ImageUnitConfig) Validate() error {
	if iuc.CustomersImagePath == "" {
		return errors.New("отсутствует customersImagePath")
	}
	if iuc.OriginalImagePathName == "" {
		return errors.New("отсутствует originalImagePathName")
	}

	err := iuc.PreviewImageUnitConfig.Validate()
	if err != nil {
		return err
	}

	return nil
}

type PreviewImageUnitConfig struct {
	PreviewImagePathName string `json:"previewImagePathName"`
	ResizeImageHeight    int    `json:"resizeImageHeight"`
	ResizeImageWidth     int    `json:"resizeImageWidth"`
}

func (iuc PreviewImageUnitConfig) Validate() error {
	if iuc.PreviewImagePathName == "" {
		return errors.New("отсутствует previewImagePathName")
	}
	if iuc.ResizeImageHeight == 0 {
		return errors.New("resizeImageHeight не может быть задан 0")
	}
	if iuc.ResizeImageWidth == 0 {
		return errors.New("resizeImageWidth не может быть задан 0")
	}

	return nil
}
