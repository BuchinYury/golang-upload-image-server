package images

import (
	"bytes"
	"image"
	"image/png"
	"os"
	"testing"
)

func TestResizeImage(t *testing.T) {
	imageUnit := ImageUnit{
		ImageUnitConfig: ImageUnitConfig{
			CustomersImagePath:    "./test_resize_Image",
			OriginalImagePathName: "original",
			PreviewImageUnitConfig: PreviewImageUnitConfig{
				PreviewImagePathName: "preview",
				ResizeImageHeight:    100,
				ResizeImageWidth:     100,
			},
		},
	}

	for _, testCase := range []struct {
		description string
		imageUnit   ImageUnit
		isErr       bool
	}{
		{
			description: "Тест на существующей картинке",
			imageUnit:   imageUnit,
			isErr:       false,
		},
		{
			description: "Тест на не картинке",
			imageUnit:   imageUnit,
			isErr:       true,
		},
	} {
		err := os.MkdirAll(testCase.imageUnit.ImageUnitConfig.CustomersImagePath, os.ModePerm)
		if err != nil {
			t.Fatal("нет возможности создать папку для теста")
		}

		if !testCase.isErr {
			resizeImageTask := ImageTask{
				ImagePath: testCase.imageUnit.ImageUnitConfig.CustomersImagePath,
				ImageName: "test_resize_image",
				ImageData: imageDataForTest(t),
			}

			resizeImageErrChan := testCase.imageUnit.ResizeImage(resizeImageTask)
			err = <-resizeImageErrChan
			if err != nil {
				t.Error("на валидном тесте произошла ошибка изменения размера картинки", err)
			}

			resizeImagePath := resizeImageTask.ImagePath +
				"/" + testCase.imageUnit.ImageUnitConfig.PreviewImageUnitConfig.PreviewImagePathName
			imageFileName := resizeImageTask.ImageName + "_resize_100x100.png"

			testCase.imageUnit.checkResizeImage(t, resizeImagePath+"/"+imageFileName)

			err = os.RemoveAll(resizeImageTask.ImagePath)
			if err != nil {
				t.Fatal("нет возможности удалить тестовую папку")
			}

		}

		if testCase.isErr {
			resizeImageTask := ImageTask{
				ImagePath: testCase.imageUnit.ImageUnitConfig.CustomersImagePath,
				ImageName: "resize_image_test.go",
				ImageData: nil,
			}
			resizeImageErrChan := testCase.imageUnit.ResizeImage(resizeImageTask)
			err = <-resizeImageErrChan
			if err == nil {
				t.Error("на не валидном тесте изменение размера картинки прошло без ошибок")
			}
		}

		err = os.RemoveAll(testCase.imageUnit.ImageUnitConfig.CustomersImagePath)
		if err != nil {
			t.Fatal("нет возможности удалить тестовую папку")
		}
	}
}

func (iu ImageUnit) checkResizeImage(t *testing.T, pathToResizeImage string) {
	resizeImageFile, err := os.Open(pathToResizeImage)
	if err != nil {
		t.Fatal("произошли проблемы при ресайзинге картинки", err)
	}
	defer resizeImageFile.Close()

	resizeImage, _, err := image.DecodeConfig(resizeImageFile)
	if err != nil {
		t.Fatal("произошли проблемы при декодтровании картинки:", err)
	}

	if iu.ImageUnitConfig.PreviewImageUnitConfig.ResizeImageHeight != resizeImage.Height ||
		iu.ImageUnitConfig.PreviewImageUnitConfig.ResizeImageWidth != resizeImage.Width {
		t.Error("неожиданный результат")
	}
}

func imageDataForTest(t *testing.T) []byte {
	img := image.NewRGBA(image.Rect(0, 0, 1000, 500))

	// create buffer
	buff := new(bytes.Buffer)

	err := png.Encode(buff, img)
	if err != nil {
		t.Fatal("нет возможности создать картинку для теста")
	}

	return buff.Bytes()
}
