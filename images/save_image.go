package images

import (
	"errors"
	serverErr "golang-upload-image-server/errors"
	"io/ioutil"
	"os"
)

type ImageSaver interface {
	SaveImage(ImageTask) <-chan error
}

func (iu ImageUnit) SaveImage(imageTask ImageTask) <-chan error {
	errChan := make(chan error)

	go func(imageTask ImageTask, errChan chan<- error) {
		imagePath := imageTask.ImagePath
		imageName := imageTask.ImageName
		imageData := imageTask.ImageData

		imageExtension, err := SupportImageExtension(imageData)
		if err != nil {
			errChan <- err
			return
		}

		originalImagePath := imagePath + "/" + iu.ImageUnitConfig.OriginalImagePathName
		err = os.MkdirAll(originalImagePath, os.ModePerm)
		if err != nil {
			errChan <- errors.New(serverErr.ImageSaveError)
			return
		}

		imageFileName := imageName + imageExtension
		err = ioutil.WriteFile(originalImagePath+"/"+imageFileName, imageData, 0644)
		if err != nil {
			errChan <- errors.New(serverErr.ImageSaveError)
			return
		}

		errChan <- nil
	}(imageTask, errChan)

	return errChan
}
