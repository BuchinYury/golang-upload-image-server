package images

import (
	"io/ioutil"
	"log"
	"os"
	"testing"
)

func TestSaveImage(t *testing.T) {
	imagePath := "./test_save_Image"

	imageUnit := ImageUnit{
		ImageUnitConfig: ImageUnitConfig{
			CustomersImagePath:    imagePath,
			OriginalImagePathName: "original",
			PreviewImageUnitConfig: PreviewImageUnitConfig{
				PreviewImagePathName: "preview",
				ResizeImageHeight:    100,
				ResizeImageWidth:     100,
			},
		},
	}

	for _, testCase := range []struct {
		description string
		fileByte    []byte
		fileExt     string
		isErr       bool
	}{
		{
			description: "Тест сохранения jpeg",
			fileByte:    []byte("\xff\xd8\xff_aaa"),
			fileExt:     jpegExt,
			isErr:       false,
		},
		{
			description: "Тест сохранения png",
			fileByte:    []byte("\x89PNG\r\n\x1a\n_aaa"),
			fileExt:     pngExt,
			isErr:       false,
		},
		{
			description: "Тест сохранения не поддерживаемых форматов",
			fileByte:    []byte("_aaa"),
			isErr:       true,
		},
	} {
		err := os.MkdirAll(imagePath, os.ModePerm)
		if err != nil {
			t.Fatal("нет возможности создать папку для теста")
		}

		resizeImageTask := ImageTask{
			ImagePath: imageUnit.ImageUnitConfig.CustomersImagePath,
			ImageName: "test_resize_image",
			ImageData: testCase.fileByte,
		}

		saveImageErrChan := imageUnit.SaveImage(resizeImageTask)
		err = <-saveImageErrChan
		if testCase.isErr && err == nil {
			t.Error("неожиданный результат")
		}
		if !testCase.isErr && err != nil {
			t.Error("неожиданный результат")
		}

		files, err := ioutil.ReadDir(imagePath)
		if err != nil {
			log.Fatal("нет возможности открыть тестовую папку")
		}

		if testCase.isErr && len(files) > 0 {
			t.Error("не валидный тест создал папки")
		}
		if !testCase.isErr && len(files) == 0 {
			t.Error("валидный тест не создал папку для картинок")
		}
		if !testCase.isErr && len(files) > 1 {
			t.Error("валидный тест создал больше одной папки для картинок")
		}
		if !testCase.isErr && len(files) == 1 {

		}

		err = os.RemoveAll(imagePath)
		if err != nil {
			t.Fatal("нет возможности удалить тестовую папку")
		}
	}
}
