package images

import (
	"io/ioutil"
	"log"
)

type Images struct {
	OriginalImagePath string
	PreviewImagePath  string
}

type ImageResponse struct {
	ImageLine []Images
}

type ImageLineListProvider interface {
	GetImageLineList() ImageResponse
}

func (iu ImageUnit) GetImageLineList() ImageResponse {

	imagesResponse := ImageResponse{}

	customerImagesDirs, err := ioutil.ReadDir(iu.ImageUnitConfig.CustomersImagePath)
	if err != nil {
		log.Fatal(err)
	}

	imageLine := make([]Images, 0)

	for _, customerImagesDir := range customerImagesDirs {
		customerImagesDirName := customerImagesDir.Name()

		if !customerImagesDir.IsDir() {
			continue
		}

		customerImagesDir, err := ioutil.ReadDir(iu.ImageUnitConfig.CustomersImagePath + "/" + customerImagesDirName)
		if err != nil {
			log.Fatal(err)
		}

		images := Images{}

		for _, imagesDir := range customerImagesDir {
			if imagesDir.Name() == iu.ImageUnitConfig.OriginalImagePathName {
				originalImage, err := ioutil.ReadDir(iu.ImageUnitConfig.CustomersImagePath + "/" + customerImagesDirName + "/" + iu.ImageUnitConfig.OriginalImagePathName)
				if err != nil {
					log.Fatal(err)
				}
				if len(originalImage) != 1 || len(originalImage) == 1 && originalImage[0].IsDir() {
					continue
				}
				images.OriginalImagePath = customerImagesDirName + "/" + iu.ImageUnitConfig.OriginalImagePathName + "/" + originalImage[0].Name()
				continue
			}

			if imagesDir.Name() == iu.ImageUnitConfig.PreviewImageUnitConfig.PreviewImagePathName {
				previewImage, err := ioutil.ReadDir(iu.ImageUnitConfig.CustomersImagePath + "/" + customerImagesDirName + "/" + iu.ImageUnitConfig.PreviewImageUnitConfig.PreviewImagePathName)
				if err != nil {
					log.Fatal(err)
				}
				if len(previewImage) != 1 || len(previewImage) == 1 && previewImage[0].IsDir() {
					continue
				}
				images.PreviewImagePath = customerImagesDirName + "/" + iu.ImageUnitConfig.PreviewImageUnitConfig.PreviewImagePathName + "/" + previewImage[0].Name()
				continue
			}
		}

		if images.OriginalImagePath == "" || images.PreviewImagePath == "" {
			continue
		}

		imageLine = append(imageLine, images)
	}

	imagesResponse.ImageLine = imageLine

	return imagesResponse
}
