package images

import (
	"strings"
	"testing"
)

func TestMagicTable(t *testing.T) {
	if jpegExt != ".jpeg" {
		t.Error("не валидное определение jpeg mime")
	}
	if pngExt != ".png" {
		t.Error("не валидное определение png mime")
	}
	if jpegMagic != "\xff\xd8\xff" {
		t.Error("не валидное определение jpeg magic byte")
	}
	if pngMagic != "\x89PNG\r\n\x1a\n" {
		t.Error("не валидное определение png magic byte")
	}
	if magicTable[jpegMagic] != jpegExt {
		t.Error("не валидное соотношение jpeg magic byte и mime")
	}
	if magicTable[pngMagic] != pngExt {
		t.Error("не валидное соотношение png magic byte и mime")
	}
}

func Test_SupportImageExtension(t *testing.T) {
	for _, testCase := range []struct {
		description string
		fileByte    []byte
		result      string
		isErr       bool
	}{
		{
			description: "Валидный тест jpeg",
			fileByte:    []byte("\xff\xd8\xff_aaa"),
			result:      jpegExt,
			isErr:       false,
		},
		{
			description: "Валидный тест png",
			fileByte:    []byte("\x89PNG\r\n\x1a\n_aaa"),
			result:      pngExt,
			isErr:       false,
		},
		{
			description: "Не валидный тест",
			fileByte:    []byte("___aaa"),
			isErr:       true,
		},
	} {
		imageExt, err := SupportImageExtension(testCase.fileByte)

		if testCase.isErr && err == nil {
			t.Error("неожиданный результат")
		}
		if !testCase.isErr && err != nil {
			t.Error("неожиданный результат")
		}

		if testCase.isErr && imageExt != "" {
			t.Error("неожиданный результат")
		}
		if !testCase.isErr && imageExt != testCase.result {
			t.Error("неожиданный результат")
		}
	}
}

func Test_GenImageMeta(t *testing.T) {
	for _, testCase := range []struct {
		imageUnit             ImageUnit
		resultImagePathPrefix string
		resultImageNamePrefix string
	}{
		{
			imageUnit: ImageUnit{

			},
			resultImagePathPrefix: "",
			resultImageNamePrefix: "image-",
		},
		{
			imageUnit: ImageUnit{
				ImageUnitConfig: ImageUnitConfig{
					CustomersImagePath: "hello",
				},
			},
			resultImagePathPrefix: "hello",
			resultImageNamePrefix: "image-",
		},
	} {
		imagePath, imageName := testCase.imageUnit.GenImageMeta()

		if !strings.HasPrefix(imagePath, testCase.resultImagePathPrefix) {
			t.Error("неожиданный результат")
		}
		if !strings.HasPrefix(imageName, testCase.resultImageNamePrefix) {
			t.Error("неожиданный результат")
		}
	}
}

func Test_Validate_ImageUnitConfig(t *testing.T) {
	for _, testCase := range []struct {
		imageUnitConfig ImageUnitConfig
		isErr           bool
	}{
		{
			imageUnitConfig: ImageUnitConfig{
				CustomersImagePath:    "client/images/customers",
				OriginalImagePathName: "original",
				PreviewImageUnitConfig: PreviewImageUnitConfig{
					PreviewImagePathName: "preview_100x100",
					ResizeImageHeight:    100,
					ResizeImageWidth:     100,
				},
			},
			isErr: false,
		},
		{
			imageUnitConfig: ImageUnitConfig{},
			isErr:           true,
		},
		{
			imageUnitConfig: ImageUnitConfig{
				CustomersImagePath:    "client/images/customers",
				OriginalImagePathName: "",
				PreviewImageUnitConfig: PreviewImageUnitConfig{
					PreviewImagePathName: "preview_100x100",
					ResizeImageHeight:    100,
					ResizeImageWidth:     100,
				},
			},
			isErr: true,
		},
		{
			imageUnitConfig: ImageUnitConfig{
				CustomersImagePath:    "client/images/customers",
				OriginalImagePathName: "original",
				PreviewImageUnitConfig: PreviewImageUnitConfig{
					PreviewImagePathName: "",
					ResizeImageHeight:    100,
					ResizeImageWidth:     100,
				},
			},
			isErr: true,
		},
		{
			imageUnitConfig: ImageUnitConfig{
				CustomersImagePath:    "client/images/customers",
				OriginalImagePathName: "original",
				PreviewImageUnitConfig: PreviewImageUnitConfig{
					PreviewImagePathName: "preview_100x100",
					ResizeImageHeight:    0,
					ResizeImageWidth:     100,
				},
			},
			isErr: true,
		},
		{
			imageUnitConfig: ImageUnitConfig{
				CustomersImagePath:    "client/images/customers",
				OriginalImagePathName: "original",
				PreviewImageUnitConfig: PreviewImageUnitConfig{
					PreviewImagePathName: "preview_100x100",
					ResizeImageHeight:    100,
					ResizeImageWidth:     0,
				},
			},
			isErr: true,
		},
	} {
		imageUnitConfigValidate := testCase.imageUnitConfig.Validate()

		if testCase.isErr && imageUnitConfigValidate == nil {
			t.Error(imageUnitConfigValidate)
			t.Error("неожиданный результат")
		}
	}
}
