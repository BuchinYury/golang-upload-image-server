package images

import (
	"bytes"
	"errors"
	"github.com/disintegration/imaging"
	serverErr "golang-upload-image-server/errors"
	"os"
	"strconv"
)

type ImageResizer interface {
	ResizeImage(ImageTask) <-chan error
}

func (iu ImageUnit) ResizeImage(imageTask ImageTask) <-chan error {
	errChan := make(chan error)

	go func(imageTask ImageTask, errChan chan<- error) {
		imagePath := imageTask.ImagePath
		imageName := imageTask.ImageName
		imageData := imageTask.ImageData

		imageExtension, err := SupportImageExtension(imageData)
		if err != nil {
			errChan <- err
			return
		}

		resizeImagePath := imagePath + "/" + iu.ImageUnitConfig.PreviewImageUnitConfig.PreviewImagePathName
		err = os.MkdirAll(resizeImagePath, os.ModePerm)
		if err != nil {
			errChan <- errors.New(serverErr.ImageSaveError)
			return
		}

		imageFileName := imageName + "_resize_" +
			strconv.Itoa(iu.ImageUnitConfig.PreviewImageUnitConfig.ResizeImageWidth) + "x" +
			strconv.Itoa(iu.ImageUnitConfig.PreviewImageUnitConfig.ResizeImageHeight) +
			imageExtension

		img, err := imaging.Decode(bytes.NewReader(imageData))
		if err != nil {
			errChan <- errors.New(serverErr.ImageSaveError)
			return
		}

		resizeImage := imaging.Resize(img,
			iu.ImageUnitConfig.PreviewImageUnitConfig.ResizeImageWidth,
			iu.ImageUnitConfig.PreviewImageUnitConfig.ResizeImageHeight,
			imaging.Box)

		err = imaging.Save(resizeImage, resizeImagePath+"/"+imageFileName)
		if err != nil {
			errChan <- errors.New(serverErr.ImageSaveError)
			return
		}

		errChan <- nil
	}(imageTask, errChan)

	return errChan
}
