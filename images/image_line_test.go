package images

import (
	"image"
	"image/png"
	"os"
	"testing"
)

func Test_getImageLineList(t *testing.T) {
	imageUnit := ImageUnit{
		ImageUnitConfig: ImageUnitConfig{
			CustomersImagePath:    "./images",
			OriginalImagePathName: "original",
			PreviewImageUnitConfig: PreviewImageUnitConfig{
				PreviewImagePathName: "preview",
				ResizeImageHeight:    100,
				ResizeImageWidth:     100,
			},
		},
	}

	for _, testCase := range []struct {
		description  string
		fileName     string
		isErr        bool
		createImages bool
	}{
		{
			description:  "Существуют папки картинок, но отсутствуют файлы картинок",
			isErr:        false,
			createImages: false,
		},
		{
			description:  "Существуют файлы картинок",
			isErr:        false,
			createImages: true,
		},
		{
			description: "Нет фаилов картинок",
			isErr:       true,
		},
	} {
		err := os.MkdirAll(imageUnit.ImageUnitConfig.CustomersImagePath, os.ModePerm)
		if err != nil {
			t.Fatal("нет возможности создать папку для теста")
		}

		if testCase.isErr {
			imagesResponse := imageUnit.GetImageLineList()
			if len(imagesResponse.ImageLine) > 0 {
				t.Error("неожиданный результат")
			}
		} else {
			testImagePath := imageUnit.ImageUnitConfig.CustomersImagePath + "/test_images"

			err := os.MkdirAll(testImagePath, os.ModePerm)
			if err != nil {
				t.Fatal("нет возможности создать папку для теста")
			}

			if !testCase.createImages {
				err := os.MkdirAll(testImagePath+"/"+imageUnit.ImageUnitConfig.OriginalImagePathName, os.ModePerm)
				if err != nil {
					t.Fatal("нет возможности создать папку для теста")
				}
				err = os.MkdirAll(testImagePath+"/"+imageUnit.ImageUnitConfig.PreviewImageUnitConfig.PreviewImagePathName, os.ModePerm)
				if err != nil {
					t.Fatal("нет возможности создать папку для теста")
				}

				imagesResponse := imageUnit.GetImageLineList()
				if len(imagesResponse.ImageLine) > 0 {
					t.Error("неожиданный результат")
				}
			} else {
				err := os.MkdirAll(testImagePath+"/"+imageUnit.ImageUnitConfig.OriginalImagePathName, os.ModePerm)
				if err != nil {
					t.Fatal("нет возможности создать папку для теста")
				}
				saveImageForTest(t, testImagePath+"/"+imageUnit.ImageUnitConfig.OriginalImagePathName+"/original.png")

				err = os.MkdirAll(testImagePath+"/"+imageUnit.ImageUnitConfig.PreviewImageUnitConfig.PreviewImagePathName, os.ModePerm)
				if err != nil {
					t.Fatal("нет возможности создать папку для теста")
				}
				saveImageForTest(t, testImagePath+"/"+imageUnit.ImageUnitConfig.PreviewImageUnitConfig.PreviewImagePathName+"/preview.png")

				imagesResponse := imageUnit.GetImageLineList()
				if len(imagesResponse.ImageLine) != 1 {
					t.Error("неожиданный результат")
				}
			}
		}

		err = os.RemoveAll(imageUnit.ImageUnitConfig.CustomersImagePath)
		if err != nil {
			t.Fatal("нет возможности удалить тестовую папку")
		}
	}
}

func saveImageForTest(t *testing.T, imagePath string) {
	img := image.NewRGBA(image.Rect(0, 0, 1000, 500))

	f, _ := os.OpenFile(imagePath, os.O_WRONLY|os.O_CREATE, 0600)
	defer f.Close()
	err := png.Encode(f, img)
	if err != nil {
		t.Fatal("нет возможности создать картинку для теста")
	}
}
