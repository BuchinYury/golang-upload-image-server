package handlers

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	serverErr "golang-upload-image-server/errors"
	"golang-upload-image-server/images"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type uploadImageResponse struct {
	Response string
}

type uploadBase64Image struct {
	Base64Image string `json:"base64Image"`
}

func (uploadBase64Image *uploadBase64Image) validate() url.Values {
	errs := url.Values{}

	base64Image := uploadBase64Image.Base64Image

	if base64Image == "" {
		errs.Add("errors", "Поле base64Image является обязательным!")
	}

	return errs
}

type UploadImageHandler struct {
	UploadImageTemplate *template.Template
	Client              http.Client
	ImageMetaGenerator  images.ImageMetaGenerator
	ImageSaver          images.ImageSaver
	ImageResizer        images.ImageResizer
}

func (uih UploadImageHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		if len(r.URL.Query()) == 0 {
			_ = uih.UploadImageTemplate.Execute(w, nil)
			return
		} else {
			imgUrl := r.URL.Query().Get("img_url")
			if imgUrl == "" {
				http.Error(w, "в запросе отсутствует параметр: img_url", http.StatusBadRequest)
				return
			} else {
				uih.saveImageFromUrl(w, imgUrl)
				return
			}
		}
	case http.MethodPost:
		if r.Form == nil {
			_ = r.ParseMultipartForm(0)
		}

		if _, ok := r.Form["form_name"]; ok {
			uih.saveImageFromMultipart(w, r)
			return
		} else {
			uih.saveImageFromBase64(w, r)
			return
		}
	}

	http.Error(w, "не запланированая работа с api", http.StatusBadRequest)
}

// region Url
func (uih UploadImageHandler) saveImageFromUrl(w http.ResponseWriter, imgUrl string) {
	_, err := url.ParseRequestURI(imgUrl)
	if err != nil {
		http.Error(w, "переданное в параметр img_url значение не является валидным url", http.StatusBadRequest)
		return
	}

	downloadImgResp, err := uih.Client.Get(imgUrl)
	if err != nil {
		http.Error(w, "не удалось загрузить картинку по переданному url", http.StatusBadRequest)
		return
	}
	defer downloadImgResp.Body.Close()

	if downloadImgResp.StatusCode != http.StatusOK {
		http.Error(w, "не удалось загрузить картинку по переданному url", http.StatusBadRequest)
		return
	}

	imageData, err := ioutil.ReadAll(downloadImgResp.Body)
	if err != nil {
		http.Error(w, "не удалось загрузить картинку по переданному пути", http.StatusBadRequest)
		return
	}

	err = uih.saveImage(imageData)
	if err != nil {
		switch err.Error() {
		case serverErr.NotSupportedImageError:
			http.Error(w, "формат не поддерживается", http.StatusBadRequest)
		case serverErr.ImageSaveError:
			http.Error(w, "проблема с сохранением картинки", http.StatusInternalServerError)
		default:
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
	}
}

// endregion

// region Multipart
func (uih UploadImageHandler) saveImageFromMultipart(w http.ResponseWriter, r *http.Request) {
	response := &uploadImageResponse{}

	imageData, err := parseMultipartFile(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)

		switch err.Error() {
		case serverErr.EmptyImageError:
			response.Response = "Не отправлена картинка на сервер"
		case serverErr.BadImageError:
			response.Response = "Проблема с получением картинки"
		}

		_ = uih.UploadImageTemplate.Execute(w, response)
		return
	}

	err = uih.saveImage(imageData)
	if err != nil {
		switch err.Error() {
		case serverErr.NotSupportedImageError:
			w.WriteHeader(http.StatusBadRequest)
			response.Response = "Формат не поддерживается"
		case serverErr.ImageSaveError:
			w.WriteHeader(http.StatusInternalServerError)
			response.Response = "Проблема с сохранением картинки"
		default:
			w.WriteHeader(http.StatusInternalServerError)
			response.Response = "Проблема с сохранением картинки"
		}
	} else {
		response.Response = "Картинка успешно загружена"
	}

	_ = uih.UploadImageTemplate.Execute(w, response)
}

func parseMultipartFile(r *http.Request) ([]byte, error) {
	file, _, err := r.FormFile("file")
	if err != nil {
		return nil, errors.New(serverErr.EmptyImageError)
	}
	defer file.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, errors.New(serverErr.BadImageError)
	}

	return fileBytes, nil
}

// endregion

// region Base64
func (uih UploadImageHandler) saveImageFromBase64(w http.ResponseWriter, r *http.Request) {
	uploadBase64Image := &uploadBase64Image{}

	defer r.Body.Close()
	if err := json.NewDecoder(r.Body).Decode(uploadBase64Image); err != nil {
		http.Error(w, "не валидный json", http.StatusUnprocessableEntity)
		return
	}

	if validErrs := uploadBase64Image.validate(); len(validErrs) > 0 {
		w.Header().Set("Content-type", "application/json")
		w.WriteHeader(http.StatusUnprocessableEntity)
		json.NewEncoder(w).Encode(validErrs)
		return
	}

	rawBase64Image := uploadBase64Image.Base64Image
	base64ImageData := rawBase64Image[strings.IndexByte(rawBase64Image, ',')+1:]
	imageData, err := base64.StdEncoding.DecodeString(base64ImageData)
	if err != nil {
		http.Error(w, "поле rawBase64Image должно выть закодировано в base64!", http.StatusUnprocessableEntity)
		return
	}

	err = uih.saveImage(imageData)
	if err != nil {
		switch err.Error() {
		case serverErr.NotSupportedImageError:
			http.Error(w, "формат не поддерживается", http.StatusBadRequest)
		case serverErr.ImageSaveError:
			http.Error(w, "проблема с сохранением картинки", http.StatusInternalServerError)
		default:
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
	}
}

// endregion

func (uih UploadImageHandler) saveImage(imageData []byte) (err error) {
	imagePath, imageName := uih.ImageMetaGenerator.GenImageMeta()

	imageTask := images.ImageTask{
		ImagePath: imagePath,
		ImageName: imageName,
		ImageData: imageData,
	}

	saveImageErrChan := uih.ImageSaver.SaveImage(imageTask)
	_ = uih.ImageResizer.ResizeImage(imageTask)

	err = <-saveImageErrChan
	return
}
