package handlers

import (
	"golang-upload-image-server/images"
	"net/http"
	"net/http/httptest"
	"testing"
)

type imageLineListProvider func() images.ImageResponse

func (illp imageLineListProvider) GetImageLineList() images.ImageResponse {
	return illp()
}

func TestIndexHandler(t *testing.T) {
	path := "./root_handler_test.go"
	req := httptest.NewRequest(http.MethodGet, "http://example.com/", nil)
	w := httptest.NewRecorder()

	imageLineListProvider := imageLineListProvider(func() images.ImageResponse {
		return images.ImageResponse{}
	})

	IndexHandler(path, imageLineListProvider)(w, req)

	if w.Code != http.StatusOK {
		t.Error("неожиданный статус ответа")
	}
}

func TestNotFoundHandler(t *testing.T) {
	path := "./root_handler_test.go"
	req := httptest.NewRequest(http.MethodGet, "http://example.com/", nil)
	w := httptest.NewRecorder()

	NotFoundHandler(path)(w, req)

	if w.Code != http.StatusNotFound {
		t.Error("неожиданный статус ответа")
	}
}

func TestRootHandlerServeHTTP(t *testing.T) {
	testRootHandler := RootHandler{
		PublicFilePath: "./",
		RootHandler: func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
		},
		NotFoundHandler: func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusNotFound)
		},
	}

	for _, testCase := range []struct {
		description      string
		testRootHandler  RootHandler
		resultStatusCode int
		request          *http.Request
	}{
		{
			description:      "Роутинг на /",
			testRootHandler:  testRootHandler,
			request:          httptest.NewRequest(http.MethodGet, "http://example.com/", nil),
			resultStatusCode: http.StatusOK,
		},
		{
			description:      "Роутинг по неизвестному пути",
			testRootHandler:  testRootHandler,
			request:          httptest.NewRequest(http.MethodGet, "http://example.com/adfadadaw", nil),
			resultStatusCode: http.StatusNotFound,
		},
	} {
		w := httptest.NewRecorder()
		testCase.testRootHandler.ServeHTTP(w, testCase.request)

		if w.Code != testCase.resultStatusCode {
			t.Error("неожиданный статус ответа")
		}
	}
}

func Test_isFileExist(t *testing.T) {
	for _, testCase := range []struct {
		description string
		fileName    string
		result      bool
	}{
		{
			description: "Фаил существует",
			fileName:    "./root_handler_test.go",
			result:      true,
		},
		{
			description: "Фаил не существует",
			fileName:    "/",
			result:      false,
		},
	} {
		if isFileExist(testCase.fileName) != testCase.result {
			t.Error("неожиданная работа функции")
		}
	}
}

func TestRootHandlerServeStatic(t *testing.T) {
	testRootHandler := RootHandler{
		PublicFilePath: "./",
		RootHandler: func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
		},
		NotFoundHandler: func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusNotFound)
		},
	}

	for _, testCase := range []struct {
		description      string
		testRootHandler  RootHandler
		resultStatusCode int
		request          *http.Request
	}{
		{
			description:      "Роутинг на существующий файл",
			testRootHandler:  testRootHandler,
			request:          httptest.NewRequest(http.MethodGet, "http://example.com/root_handler_test.go", nil),
			resultStatusCode: http.StatusOK,
		},
		{
			description:      "Роутинг на не существующий файл",
			testRootHandler:  testRootHandler,
			request:          httptest.NewRequest(http.MethodGet, "http://example.com/adfadadaw.go", nil),
			resultStatusCode: http.StatusNotFound,
		},
	} {
		w := httptest.NewRecorder()
		testCase.testRootHandler.ServeStatic(w, testCase.request)

		if w.Code != testCase.resultStatusCode {
			t.Error("неожиданный статус ответа")
		}
	}
}
