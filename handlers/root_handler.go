package handlers

import (
	"golang-upload-image-server/images"
	"html/template"
	"net/http"
	"os"
	"path"
	"path/filepath"
)

type RootHandler struct {
	PublicFilePath  string
	RootHandler     http.HandlerFunc
	NotFoundHandler http.HandlerFunc
}

func (rh RootHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/" {
		rh.RootHandler(w, r)
		return
	}
	rh.ServeStatic(w, r)
}

func IndexHandler(indexTemplatePath string, imageLineListProvider images.ImageLineListProvider) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")

		imagesResponse := imageLineListProvider.GetImageLineList()

		tmpl := template.Must(template.ParseFiles(indexTemplatePath))
		_ = tmpl.Execute(w, imagesResponse)
	}
}

func NotFoundHandler(notFoundTemplatePath string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
		tmpl := template.Must(template.ParseFiles(notFoundTemplatePath))
		_ = tmpl.Execute(w, nil)
	}
}

func (rh RootHandler) ServeStatic(w http.ResponseWriter, r *http.Request) {
	upath := r.URL.Path
	upath = path.Clean(upath)
	name := path.Join(rh.PublicFilePath, filepath.FromSlash(upath))

	if !isFileExist(name) {
		rh.NotFoundHandler(w, r)
		return
	}

	http.ServeFile(w, r, name)
}

func isFileExist(name string) bool {
	file, err := os.Open(name)
	if err != nil {
		return false
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		return false
	}

	if fileInfo.IsDir() {
		return false
	}

	return true
}
