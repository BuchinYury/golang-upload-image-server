package handlers

import (
	"bytes"
	"golang-upload-image-server/images"
	"html/template"
	"image"
	"image/png"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type imageMetaGenerator func() (imagePath string, imageName string)

func (img imageMetaGenerator) GenImageMeta() (imagePath string, imageName string) {
	return img()
}

type imageSaver func(images.ImageTask) <-chan error

func (is imageSaver) SaveImage(imageTask images.ImageTask) <-chan error {
	return is(imageTask)
}

type imageResizer func(images.ImageTask) <-chan error

func (ir imageResizer) ResizeImage(imageTask images.ImageTask) <-chan error {
	return ir(imageTask)
}

func TestUploadImageHandler_ServeHTTP_GetMethod(t *testing.T) {
	uploadImageUrl := "http://example.com/upload_image"
	validImgPath := "/good_img"
	notValidImgPath := "/bad_img"
	saveImageFromUrlTestServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == validImgPath {
			img := image.NewRGBA(image.Rect(0, 0, 1000, 500))
			_ = png.Encode(w, img)
			return
		}
		if r.URL.Path == notValidImgPath {
			w.Write([]byte("is not image - is text"))
			return
		}

		w.WriteHeader(http.StatusInternalServerError)
	}))

	imageMetaGenerator := imageMetaGenerator(func() (imagePath string, imageName string) {
		return "imagePath", "imageName"
	})

	imageSaver := imageSaver(func(imageTask images.ImageTask) <-chan error {
		errChan := make(chan error)

		go func(imageTask images.ImageTask, errChan chan<- error) {
			_, err := images.SupportImageExtension(imageTask.ImageData)
			if err != nil {
				errChan <- err
				return
			}
			errChan <- nil
		}(imageTask, errChan)

		return errChan
	})

	imageResizer := imageResizer(func(imageTask images.ImageTask) <-chan error {
		errChan := make(chan error)

		go func(imageTask images.ImageTask, errChan chan<- error) {
			_, err := images.SupportImageExtension(imageTask.ImageData)
			if err != nil {
				errChan <- err
				return
			}
			errChan <- nil
		}(imageTask, errChan)

		return errChan
	})

	for _, getMethodTestCase := range []struct {
		description            string
		testUploadImageHandler UploadImageHandler
		resultStatusCode       int
		request                *http.Request
	}{
		{
			description: "Запрос на получение страницы загрузки картинок",
			testUploadImageHandler: UploadImageHandler{
				UploadImageTemplate: template.Must(template.ParseFiles("./upload_image_handler_test.go")),
			},
			request: httptest.NewRequest(http.MethodGet,
				uploadImageUrl,
				nil),
			resultStatusCode: http.StatusOK,
		},
		{
			description:            "Запрос на получение картинки по Get параметру. Без img_url.",
			testUploadImageHandler: UploadImageHandler{},
			request: httptest.NewRequest(http.MethodGet,
				uploadImageUrl+"?a=a",
				nil),
			resultStatusCode: http.StatusBadRequest,
		},
		{
			description:            "Запрос на получение картинки по Get параметру. С не валидным img_url.",
			testUploadImageHandler: UploadImageHandler{},
			request: httptest.NewRequest(http.MethodGet,
				uploadImageUrl+"?img_url=a",
				nil),
			resultStatusCode: http.StatusBadRequest,
		},
		{
			description:            "Запрос на получение картинки по Get параметру. С валидным img_url, запрос по которому вернется со статусом 500.",
			testUploadImageHandler: UploadImageHandler{},
			request: httptest.NewRequest(http.MethodGet,
				uploadImageUrl+"?img_url="+saveImageFromUrlTestServer.URL,
				nil),
			resultStatusCode: http.StatusBadRequest,
		},
		{
			description: "Запрос на получение картинки по Get параметру. С валидным img_url, запрос по которому вернет валидную картинку.",
			testUploadImageHandler: UploadImageHandler{
				ImageMetaGenerator: imageMetaGenerator,
				ImageSaver:         imageSaver,
				ImageResizer:       imageResizer,
			},
			request: httptest.NewRequest(http.MethodGet,
				uploadImageUrl+"?img_url="+saveImageFromUrlTestServer.URL+validImgPath,
				nil),
			resultStatusCode: http.StatusOK,
		},
		{
			description: "Запрос на получение картинки по Get параметру. С валидным img_url, запрос по которому вернет не валидную картинку.",
			testUploadImageHandler: UploadImageHandler{
				ImageMetaGenerator: imageMetaGenerator,
				ImageSaver:         imageSaver,
				ImageResizer:       imageResizer,
			},
			request: httptest.NewRequest(http.MethodGet,
				uploadImageUrl+"?img_url="+saveImageFromUrlTestServer.URL+notValidImgPath,
				nil),
			resultStatusCode: http.StatusBadRequest,
		},
	} {
		w := httptest.NewRecorder()
		getMethodTestCase.testUploadImageHandler.ServeHTTP(w, getMethodTestCase.request)

		if w.Code != getMethodTestCase.resultStatusCode {
			t.Error("неожиданный статус ответа")
		}
	}
}

func TestUploadImageHandler_ServeHTTP_PostMethod(t *testing.T) {
	uploadImageUrl := "http://example.com/upload_image"

	imageMetaGenerator := imageMetaGenerator(func() (imagePath string, imageName string) {
		return "imagePath", "imageName"
	})

	imageSaver := imageSaver(func(imageTask images.ImageTask) <-chan error {
		errChan := make(chan error)

		go func(imageTask images.ImageTask, errChan chan<- error) {
			_, err := images.SupportImageExtension(imageTask.ImageData)
			if err != nil {
				errChan <- err
				return
			}
			errChan <- nil
		}(imageTask, errChan)

		return errChan
	})

	imageResizer := imageResizer(func(imageTask images.ImageTask) <-chan error {
		errChan := make(chan error)

		go func(imageTask images.ImageTask, errChan chan<- error) {
			_, err := images.SupportImageExtension(imageTask.ImageData)
			if err != nil {
				errChan <- err
				return
			}
			errChan <- nil
		}(imageTask, errChan)

		return errChan
	})

	multipartRequestValidImage := func() *http.Request {
		img := image.NewRGBA(image.Rect(0, 0, 1000, 500))

		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)

		writer.WriteField("form_name", "hello")
		part, _ := writer.CreateFormFile("file", "hello.png")
		png.Encode(part, img)
		writer.Close()

		r, _ := http.NewRequest(http.MethodPost, uploadImageUrl, body)
		r.Header.Add("Content-Type", writer.FormDataContentType())

		return r
	}()

	multipartRequestEmptyImage := func() *http.Request {
		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)

		writer.WriteField("form_name", "hello")
		writer.Close()

		r, _ := http.NewRequest(http.MethodPost, uploadImageUrl, body)
		r.Header.Add("Content-Type", writer.FormDataContentType())

		return r
	}()

	multipartRequestNotValidImage := func() *http.Request {
		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)

		writer.WriteField("form_name", "hello")
		part, _ := writer.CreateFormFile("file", "hello.png")
		part.Write([]byte("is not image - is text"))
		writer.Close()

		r, _ := http.NewRequest(http.MethodPost, uploadImageUrl, body)
		r.Header.Add("Content-Type", writer.FormDataContentType())

		return r
	}()

	for _, getMethodTestCase := range []struct {
		description            string
		testUploadImageHandler UploadImageHandler
		resultStatusCode       int
		request                *http.Request
	}{
		{
			description:            "POST запрос. Пустое тело",
			testUploadImageHandler: UploadImageHandler{},
			request: httptest.NewRequest(http.MethodPost,
				uploadImageUrl,
				nil),
			resultStatusCode: http.StatusUnprocessableEntity,
		},
		{
			description:            "POST запрос. Тело c пустым json",
			testUploadImageHandler: UploadImageHandler{},
			request: httptest.NewRequest(http.MethodPost,
				uploadImageUrl,
				strings.NewReader("{}")),
			resultStatusCode: http.StatusUnprocessableEntity,
		},
		{
			description:            "POST запрос. Тело - json с пустым полем base64Image",
			testUploadImageHandler: UploadImageHandler{},
			request: httptest.NewRequest(http.MethodPost,
				uploadImageUrl,
				strings.NewReader("{\"base64Image\":\"\"}")),
			resultStatusCode: http.StatusUnprocessableEntity,
		},
		{
			description:            "POST запрос. Тело - json с невалидным полем base64Image",
			testUploadImageHandler: UploadImageHandler{},
			request: httptest.NewRequest(http.MethodPost,
				uploadImageUrl,
				strings.NewReader("{\"base64Image\":\"111\"}")),
			resultStatusCode: http.StatusUnprocessableEntity,
		},
		{
			description: "POST запрос. Тело - json с валидным полем base64Image, в которм закодирована не картинка",
			testUploadImageHandler: UploadImageHandler{
				ImageMetaGenerator: imageMetaGenerator,
				ImageSaver:         imageSaver,
				ImageResizer:       imageResizer,
			},
			request: httptest.NewRequest(http.MethodPost,
				uploadImageUrl,
				strings.NewReader("{\"base64Image\":\"MTEx\"}")),
			resultStatusCode: http.StatusBadRequest,
		},
		{
			description: "POST запрос. Тело - json с валидным полем base64Image, в которм закодирована картинка",
			testUploadImageHandler: UploadImageHandler{
				ImageMetaGenerator: imageMetaGenerator,
				ImageSaver:         imageSaver,
				ImageResizer:       imageResizer,
			},
			request: httptest.NewRequest(http.MethodPost,
				uploadImageUrl,
				strings.NewReader("{\"base64Image\":\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAFCAYAAAB8ZH1oAAAAE0lEQVR4nGJiIBIMBYWAAAAA//8FmwAL7JiQygAAAABJRU5ErkJggg==\"}")),
			resultStatusCode: http.StatusOK,
		},
		{
			description: "POST запрос. Тело - multipart/form-data с валидной картинкой в поле file",
			testUploadImageHandler: UploadImageHandler{
				UploadImageTemplate: template.Must(template.ParseFiles("./upload_image_handler_test.go")),
				ImageMetaGenerator:  imageMetaGenerator,
				ImageSaver:          imageSaver,
				ImageResizer:        imageResizer,
			},
			request:          multipartRequestValidImage,
			resultStatusCode: http.StatusOK,
		},
		{
			description: "POST запрос. Тело - multipart/form-data с пустым полем file",
			testUploadImageHandler: UploadImageHandler{
				UploadImageTemplate: template.Must(template.ParseFiles("./upload_image_handler_test.go")),
				ImageMetaGenerator:  imageMetaGenerator,
				ImageSaver:          imageSaver,
				ImageResizer:        imageResizer,
			},
			request:          multipartRequestEmptyImage,
			resultStatusCode: http.StatusBadRequest,
		},
		{
			description: "POST запрос. Тело - multipart/form-data с не валидной картинкой в поле file",
			testUploadImageHandler: UploadImageHandler{
				UploadImageTemplate: template.Must(template.ParseFiles("./upload_image_handler_test.go")),
				ImageMetaGenerator:  imageMetaGenerator,
				ImageSaver:          imageSaver,
				ImageResizer:        imageResizer,
			},
			request:          multipartRequestNotValidImage,
			resultStatusCode: http.StatusBadRequest,
		},
	} {
		w := httptest.NewRecorder()
		getMethodTestCase.testUploadImageHandler.ServeHTTP(w, getMethodTestCase.request)

		if w.Code != getMethodTestCase.resultStatusCode {
			t.Error("неожиданный статус ответа")
		}
	}
}

func TestUploadImageHandler_ServeHTTP_NotUseMethod(t *testing.T) {
	for _, notUseMethodTestCase := range []struct {
		description            string
		testUploadImageHandler UploadImageHandler
		resultStatusCode       int
		request                *http.Request
	}{
		{
			description:            "Запрос с методом не равным Get или Post",
			testUploadImageHandler: UploadImageHandler{},
			request:                httptest.NewRequest(http.MethodPut, "http://example.com/upload_image", nil),
			resultStatusCode:       http.StatusBadRequest,
		},
	} {
		w := httptest.NewRecorder()
		notUseMethodTestCase.testUploadImageHandler.ServeHTTP(w, notUseMethodTestCase.request)

		if w.Code != notUseMethodTestCase.resultStatusCode {
			t.Error("неожиданный статус ответа")
		}
	}
}
